#if 0
#include <gst/gst.h>
#include <glib.h>
//定义消息处理函数,
static gboolean bus_call(GstBus *bus,GstMessage *msg,gpointer data)
{
    GMainLoop *loop = (GMainLoop *) data;//这个是主循环的指针，在接受EOS消息时退出循环
    switch (GST_MESSAGE_TYPE(msg))
    {
        case GST_MESSAGE_EOS:
            g_print("End of stream\n");
            g_main_loop_quit(loop);
            break;
        case GST_MESSAGE_ERROR:
        {
                           gchar *debug;
                           GError *error;

                           gst_message_parse_error(msg,&error,&debug);
                           g_free(debug);
                           g_printerr("ERROR:%s\n",error->message);
                           g_error_free(error);
                           g_main_loop_quit(loop);
                           break;
        }
        default:
             break;
    }
    return TRUE;
}

int main(int argc,char *argv[])
{
    GMainLoop *loop;
    GstElement *pipeline,*source,*decoder,*sink;//定义组件
    GstBus *bus;

    gst_init(&argc,&argv);
    loop = g_main_loop_new(NULL,FALSE);//创建主循环，在执行 g_main_loop_run后正式开始循环

    if(argc != 3)
    {
        g_printerr("Usage:%s <mp3 filename> <yuv filename>\n",argv[0]);
        return -1;
    }
    //创建管道和组件
    pipeline = gst_pipeline_new("video-player");
    source = gst_element_factory_make("filesrc","file-source");
	decoder = gst_element_factory_make ("decodebin", "convert");
    //sink = gst_element_factory_make("autovideosink","video-output");
	sink = gst_element_factory_make("filesink","video-output");
	
    if(!pipeline||!source||!decoder||!sink){
        g_printerr("One element could not be created.Exiting.\n");
        return -1;
    }
    //设置 source的location 参数。即 文件地址.
    g_object_set(G_OBJECT(source),"location",argv[1],NULL);
	g_object_set(G_OBJECT(sink),"location",argv[2],NULL);
    //得到 管道的消息总线
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
   //添加消息监视器
    gst_bus_add_watch(bus,bus_call,loop);
    gst_object_unref(bus);
    //把组件添加到管道中.管道是一个特殊的组件，可以更好的让数据流动
    gst_bin_add_many(GST_BIN(pipeline),source,decoder,sink,NULL);
   //依次连接组件
   gst_element_link_many(source,decoder,sink,NULL);
   //开始播放
    gst_element_set_state(pipeline,GST_STATE_PLAYING);
    g_print("Running\n");
    //开始循环
    g_main_loop_run(loop);
    g_print("Returned,stopping playback\n");
    gst_element_set_state(pipeline,GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(pipeline));
    return 0;
}
#endif

#include <gst/gst.h>
#include <glib.h>
#include <stdio.h>

#include <gst/app/gstappsrc.h>
static FILE * appSrcFile = NULL;
static FILE * appdstFile = NULL;

static int  read_counter = 0;
static char read_buffer[4096];
 

#define buffer_size 4096
static void cb_need_data (GstElement *source, guint       unused_size, gpointer    user_data)
{
    GstBuffer *buffer;
    GstFlowReturn ret;
    GstMapInfo map;
 	int size=buffer_size;
    //g_print("%s\n", __func__);
 
    /* (appSrcFile == NULL)
    {
       appSrcFile = fopen("sample_720p.h264", "r");
    }*/
 
    size = fread(read_buffer, 1, size, appSrcFile);
    //g_print("read_data() read_counter=%d, size=%d\n", read_counter++, size);
 
    if(size == 0)
    {
        ret = gst_app_src_end_of_stream(source);
        g_print("eos returned %d at %d\n", ret, __LINE__);
        return;
    }
 
    buffer = gst_buffer_new_allocate (NULL, size, NULL);
//这两个方法都可以
#if 1
    gst_buffer_fill(buffer, 0, read_buffer, size);
#else
    gst_buffer_map (buffer, &map, GST_MAP_WRITE);
    memcpy( (guchar *)map.data, read_buffer, gst_buffer_get_size( buffer ) );
	gst_buffer_unmap(buffer, &map);
#endif
 
    g_signal_emit_by_name (source, "push-buffer", buffer, &ret);
    gst_buffer_unref (buffer);
}

/* The appsink has received a buffer */
static GstFlowReturn new_sample (GstElement *sink, gpointer    user_data) {
  GstSample *sample;
  GstBuffer *buffer;
  /* Retrieve the buffer */
  g_signal_emit_by_name (sink, "pull-sample", &sample);
  //sample = gst_app_sink_pull_sample (sink);
  GstMapInfo map;
  GstFlowReturn ret;
  /*if(sample == NULL){
		if(gst_app_sink_is_eos(sink))
			return GST_FLOW_EOS;
	}*/
	if(sample){
		buffer = gst_sample_get_buffer (sample);
		g_print("on_new_sample_from_sink() call!; size = %d\n", gst_buffer_get_size(buffer));
	}
	else{
		g_print("sample is NULL \n");
		return ret;
	}

	 /* Mapping a buffer can fail (non-readable) */
    if (gst_buffer_map (buffer, &map, GST_MAP_READ)) {
        /* print the buffer data for debug */
        /*int i = 0, j = 0;
        for(; i < 10; i++)         
              g_print("%x ", map.data[i]);
        g_print("\n");*/
        gint width,height;
        GstCaps *caps = gst_sample_get_caps(sample);
        GstStructure *structure = gst_caps_get_structure(caps,0);
	    gst_structure_get_int(structure,"width",&width);
	    gst_structure_get_int(structure,"height",&height);
		fwrite(map.data, 1, width*height*3/2, appdstFile);
		g_print("width = %d  height = %d\n", width,height);
    }
	gst_sample_unref (sample);
	return GST_FLOW_OK;
}

 
gint main (gint   argc, gchar *argv[])
{
	GstElement *pipeline, *appsrc, *conv, *parse, *videosink;
	if(argc != 3)
	{
	    g_printerr("Usage:%s <mp4 filename>  <out filename>\n",argv[0]);
	    return -1;
	}
	if (appSrcFile == NULL)
	{
	   appSrcFile = fopen(argv[1], "r");
	}
	if (appdstFile == NULL)
	{
	   appdstFile = fopen(argv[2], "wb");
	}
	printf("111111\r\n");
	/* init GStreamer */
	gst_init (NULL, NULL);
	GMainLoop* loop = g_main_loop_new (NULL, FALSE);

	/* setup pipeline */
	/*pipeline  = gst_pipeline_new ("pipeline");
	appsrc    = gst_element_factory_make ("appsrc",       "source");
	parse      = gst_element_factory_make ("h264parse", " parse");
	conv      = gst_element_factory_make ("amlvdec", " convert");
	//videosink = gst_element_factory_make ("autovideosink",  "videosink");
	videosink = gst_element_factory_make ("appsink",  "sink");
	
	if(!appsrc || !conv || !parse || !videosink)
	{
		printf("Not all element could be created.\r\n");
		return -1;
	}*/
	
	GError *error = NULL;
	gchar *descr = g_strdup_printf ("appsrc name=source ! h264parse ! v4l2h264dec ! appsink name=sink ");

	pipeline = gst_parse_launch (descr, &error);

	if (error != NULL) {
	  g_print ("could not construct pipeline: %s\n", error->message);
	  g_clear_error (&error);
	  exit (-1);
	}

	appsrc = gst_bin_get_by_name (GST_BIN (pipeline), "source");	
	videosink = gst_bin_get_by_name (GST_BIN (pipeline), "sink");
	/*gst_bin_add_many (GST_BIN (pipeline), appsrc,parse, conv, videosink, NULL);
	gst_element_link_many (appsrc, parse, conv, videosink, NULL);*/

	//AppSrc可以工作在俩种模式下：Pull模式和Push模式。Pull模式下，AppSrc会在需要的时候向应用
	//程序请求数据（信号：need-data)，而Push模式下，应用程序主动向AppSrc注入数据(信号：enough-data)。此时会触发回调函数的执行，然后在回调函数里面解析视屏文件然后播放出来。
	/* setup appsrc */
	
	g_signal_connect (appsrc, "need-data",   G_CALLBACK (cb_need_data), NULL);
	g_object_set(     appsrc, "stream-type", GST_APP_STREAM_TYPE_STREAM, NULL );

	g_object_set (videosink, "emit-signals", TRUE, "sync", FALSE, NULL);
	g_signal_connect (videosink, "new_sample",   G_CALLBACK (new_sample), NULL);
	/* play */
	gst_element_set_state (pipeline, GST_STATE_PLAYING);
	g_main_loop_run (loop);

	/* clean up */
	gst_element_set_state (pipeline, GST_STATE_NULL);
	gst_object_unref (GST_OBJECT (pipeline));
	g_main_loop_unref (loop);
	fclose(appSrcFile);
	fclose(appdstFile);
	return 0;
}


